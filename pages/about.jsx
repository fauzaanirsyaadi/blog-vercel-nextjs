import Link from 'next/link';
import Navbar from '../components/Navbar';
import Head from 'next/head'

function About() {
	return (
		<>
			<Head>
			  <title>About</title>
			</Head>

			<Navbar />

			<section className="hero">
			  <div className="container">
			    <div className="text-wrapper w-full">
			      <h1 className="title">About</h1>
			      <p className="description">
			      	I have a degree in telecommunications engineering, University of Telkom Bandung.
					have the ability to computational thinking and algorithms and data structur. 
					Familiar with JavaScript and Python programming languages, and figma for design. 
					can speak English well (written - spoken). 
					
					interested in website development (frontend) with reactjs and wordpress.have a strong ability on git.
					
			      </p>
		      </div>
	      </div>
			</section>
		</>
	);
}

export default About;